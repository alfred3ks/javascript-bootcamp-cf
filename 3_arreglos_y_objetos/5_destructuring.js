/*
🎯🎯 Desestructuring assignment:
Podemos asignar las propiedades de un objeto a variables independientes usando lo que se conoce como asignacion por desestructuracion o destructuring assignment como lo encontramos en la documentacion.

Para asignar de esta manera debemos colocar del lado izquierdo del operador de asignacion las variable que vamos a utilizar o las propiedades en la que se va a separar el objeto, del lado derecho va el objeto del cual vamos a optener esas propiedades:

let user = {
  name: 'Uriel',
  age: 20,
};

let { name } = user;

Es como si estuvieramos creando una nueva variable que viene del objeto.

*/

// Declaramos un objeto: {}

let usuario = {
  nombre: 'Uriel',
  edad: 20,
};

let { nombre } = usuario;
console.log(nombre); // Uriel, el name se busca en el objeto la propiedad.

// Tambien podemos guardar en una variable la propiedad del objeto:
const { nombre: nombreUsuario } = usuario;
console.log(nombreUsuario);

// Tambien podemos poner para propiedades que no existan valores por defecto:
let { apellido = 'Molina' } = usuario;
console.log(apellido);

console.log(usuario);

/*
🎈🎈 No se altera el objeto original. Porque esta propiedad no exite en el objeto. Es para que no error toma el valor por defecto puesto por nosotros.
*/

// Nuevo objeto con todos los valores usando spread operator (...):
let { nombre: nombreUsuario1, ...sobrantes } = usuario;
console.log(nombreUsuario1);
console.log(sobrantes);

// Tambien lo podemos hacer cuando trabajamos con funciones pasamos el objeto desestructurado por parametro: Desempaquemos el objeto:
function saludar({ nombre }) {
  console.log('Hola ' + nombre);
}
saludar(usuario);

// Tambien podemos renombrar la variable para usarla:
function saludar1({ nombreUsuario: nombre }) {
  console.log('Hola ' + nombreUsuario);
}
saludar1(usuario);

/*
🎈🎈 Tambien podemos usar la sintaxis de destructuración sobre arreglos:
*/

let calificaciones = [10, 9, 8];
let [matematicas, programacion, ciencias] = calificaciones;

/*
Lo que se hace es asignar una variable al valor del arreglo: []
*/
console.log(matematicas); // 10
console.log(ciencias); // 8

// Ahora veamos como seria usando spread operator:
let [mate, ...resto] = calificaciones;
console.log(mate); // 10
console.log(resto); // [9,8]

// Combinando con funciones tambien:
function obtenerCalificaciones() {
  return [10, 9, 8];
}

let [matem, ...rest] = obtenerCalificaciones();

console.log(rest); // [9,8]
console.log(matem); //10
