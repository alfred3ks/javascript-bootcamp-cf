/*
🎯🎯 Arreglos: []:
Los arreglos son colecciones de datos ordenados a los que podemos identificar por su indice, comunmente su posición en el arreglo.

La manera de declarar un arreglo:

let arr1 = [];

Como los arreglos son objetos, los podemos construir usando el constructor: Ambas es lo mismo.

let arr2 = new Array();
let arr3 = Array();

Aunque esto es poco comun. Lo normal es la primera opción.

Caracteristicas de los arreglos en JavaScript:

- Tamaño dinámico:
A diferencia de lenguajes de bajo nivel, no necesitamos reservar memoria o definir de que tamaño sera el arreglo, el motor de ejecución determinará como guardará un arreglo.

- Empiezan en indice cero (0):
El primer elemento de un arreglo esta en la posición cero, por lo que el conteo de índices siempre inicia en ese valor.

- Los elementos pueden ser de cualquier tipo:
A diferencia de lenguajes de bajo nivel donde un arreglo sólo puede contener datos del mismo tipo, en JavaScript un mismo arreglo puede contener datos de distintos tipos.

- Los elementos dentro del arreglo se separan por comas.

*/

/*
🎯🎯 Obtener valores de un arreglo:
Los valores de un arreglo se obtienen por su índice, es la posición que ocupan dentro del arreglo.

let calificaciones = [10, 9, 8];
calificaciones[0] -> 10
calificaciones[1] -> 9
calificaciones[2] -> 8

Si intento acceder a un elemento que no existe dentro del arreglo recibir undefined.

calificaciones[10] -> undefined

🎈🎈 Todos los arreglos poseen un propiedad llamada length, la cual nos permite saber el tamaño del areglo.

calificaciones.length; -> 3

*/

let calificaciones = [10, 9, 8];
console.log(calificaciones[0]); // 10
console.log(calificaciones.length); // 3

/*
🎯🎯 Recorre un arreglo:
Podemos usar ciclos para recorrer los arreglos.
ciclo for(). Por ejemplo.
for of.

NOTA: El ultimo indice de un arreglo es su longitud -1.

*/

for (let i = 0; i < calificaciones.length; i++) {
  console.log('dataArr: ' + calificaciones[i]);
}

for (let calificacion of calificaciones) {
  console.log('data: ' + calificacion);
}

for (let index in calificaciones) {
  console.log('Indices: ' + index);
}
