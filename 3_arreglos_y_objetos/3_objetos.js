/*
🎯🎯 Objetos / JSON: {}
Una de las formas más sencillas de declarar un objeto es usar algo que se conoce como el JavaScript Object Notation o JSON como normalmente nos referimos a esta notación.

Para declarar un objeto lo hacemos con las llaves {}: La colección incluye una colección de pares clave:valor. Todos estos pares se separan por comas.

let usuario = {
  nombre: "Uriel",
  edad: 29,
  saludar: function(){
console.log("Hola soy Uriel"); }
}

Según json.org los objetos JSON de JavaScript es como llamamos a los diccionarios o hashes en otros lenguajes de programación, por lo que puedes concluir que un objeto JSON es lo que un diccionario en Python, o lo que un HashMap en JAVA.

🎈🎈 Leer valores de un objeto:

Hay dos formas de leer y asignar propiedades a un objeto:

- La sintaxis de corchetes.
- La sintaxis del punto.

*/

let usuario = {
  nombre: 'Mariana',
  edad: 29,
  saludar: function () {
    console.log('Hola soy ' + this.nombre);
  },
};

console.log(usuario['nombre']);
usuario['nombre'] = 'Layka';

// Accedemos a las propiedad por medio de los corchetes []: Computer property:
console.log(usuario['nombre']);

// Con la síntaxis del punto:
console.log(usuario.nombre);

/*
🎯🎯 Recorrer un objeto: for in:
Un objeto JSON es una colección de propiedades y valores. clave:valor
El ciclo for in nos permite iterar sobre las propiedades enumerables de un objeto.
*/

for (let clave in usuario) {
  console.log(usuario[clave]);
}

/*
Para aprender a diferenciar entre el ciclo for in, y el ciclo for of, recuerda lo siguiente:

- for of es para iterables -> arreglos [] 😮😮
- for in es para objetos -> para objetos {} 😮😮 🤡🤡

*/
