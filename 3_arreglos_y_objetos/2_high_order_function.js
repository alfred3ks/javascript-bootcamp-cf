/*
🎯🎯 High Order Function: 🎈🎈🎈🎈
Llamamos High Order Functions a las funciones que reciben una función como argumento o retornan una función como resultado.

En un lenguaje como JavaScript donde las funciones se consideran objetos de primera clase, las funciones pueden ser enviadas como argumento, almacenadas en variables y pueden ser el resultado (return) de una función.

El patrón de funciones de alto orden nos permite escribir funciones dinámicas que puedan modificar su comportamiento según la función que reciban.

Escribe una función que reciba 2 valores y pueda ejecutar cualquier operación matemática que retorne un número como resultado, usando los 2 valores anteriores.
*/

function calculadoraUniversal(n1, n2, operacion) {
  return operacion(n1, n2);
}

const suma = calculadoraUniversal(10, 20, (a, b) => a + b);
console.log(suma);

const resta = calculadoraUniversal(10, 20, (a, b) => a - b);
console.log(resta);

const producto = calculadoraUniversal(10, 20, (a, b) => a * b);
console.log(producto);

const potencia = calculadoraUniversal(10, 20, (a, b) => Math.pow(a, b));
console.log(potencia);

/*
🎯🎯 High Order Functions en Arreglos: []
Las funciones como map(), filter(), reduce(), forEach(), de los arreglos son ejemplos de High Order Functions luego de que, principalmente trabajan con una función que reciben como argumento.

forEach(): Tambien es otra forma de recorrer el arreglo:
forEach recibe una función como argumento, esta función se ejecuta una vez por cada elemento que hay dentro del arreglo.
*/

let data = [22, 5, 78, 'hola', true];
data.forEach((element, indice) => {
  console.log('indice:' + indice);
  console.log(element);
});

/*
🎯🎯 map():
map recibe como argumento una función, y retorna un nuevo arreglo. La premisa es que la función recibida servirá como una función de transformación para cada elemento en el arreglo.
*/

const numeros = [1, 3, 6, 9, 12, 45, 20];
let cuadrados = numeros.map((numero, index) => {
  console.log('index: ' + index);
  return numero * numero;
});

console.log(`Los numeros al cuadrado son ${cuadrados}`);
console.log(cuadrados);

/*
🎯🎯 filter():
La operación filter recibe como argumento una función, esta función se aplica para cada elemento del arreglo, el resultado de la función se evalúa como true o false, si es verdadero el elemento se conserva, si es falso, se descarta. El resultado es un nuevo arreglo con los que cumplen la condición.
*/

let pares = numeros.filter((numero) => {
  if (numero % 2 === 0) {
    return true;
  }
});

console.log(`Los numeros pares son ${pares}`);
console.log(pares);

/*
🎯🎯 reduce(): Método muy interesante:
En términos técnicos, reduce nos permite aplicar una operación de reducción para un arreglo, y así reducirlo a un solo valor. Vemos que el primera valor pasado es un acumulador, que para este caso es cero.
*/

// Cálcular el valor unico del arreglo:
let unicoValor = numeros.reduce((acumulado, numero) => {
  return acumulado + numero;
}, 0);

console.log(`El valor del array de numeros reducido es ${unicoValor}`);
console.log(unicoValor);

// Cálcular el numero mayor del arreglo:
let mayor = numeros.reduce((acc, numero) => {
  console.log(acc + '-' + numero);
  return numero > acc ? numero : acc;
}, 0);

console.log(mayor);

/*🎯🎯 Composición de funciones:
Una de las ventajas de usar High Order Functions es la 🎈🎈composición de funciones🎈🎈.
Composición de funciones es combinar distintas funciones en un orden secuencial para ejecutar una operación.
Veámoslo funcionando.Dado un arreglo obtengamos la suma de los números pares.
*/

const arregloNumeros = ['2', '4', '6', '3', '78', '23', '12', '90', '40'];

// 😮😮 Primera solución sin funciones de High Order Functions:

let summa = 0;
for (let item of arregloNumeros) {
  let itemInt = parseInt(item);
  if (itemInt % 2 === 0) {
    summa += itemInt;
  }
}
console.log(summa);

// 😮😮 Segunda solución con funciones High Order Functions: Concatenando las funciones HFO:

let sumaPares = arregloNumeros
  .map((numero) => parseInt(numero))
  .filter((numero) => numero % 2 === 0)
  .reduce((acc, numero) => acc + numero, 0);

console.log(sumaPares);

/*
En JavaScript moderno es muy común que reemplacemos el uso de ciclos para trabajar con arreglos, por el uso de funciones 🎈🎈-HFO-🎈🎈 como forEach(), map(), reduce(), entre otras, esto luego de que dichas funciones promueven la legibilidad y expresividad del código.
*/
