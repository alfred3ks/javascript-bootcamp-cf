/*
🎯🎯 Object initializer:
Aunque lucen similares, la sintaxis para inicializar un objeto no es igual a JSON.

Comúnmente nos referimos a este como un objeto JSON:

let usuario = {
  nombre: "Mariana",
  edad: 29,
  saludar: function () {
    console.log("Hola soy " + this.nombre);
  }
}

Y en la práctica describirlo como tal es correcto, es parte de cómo nos comunicamos en la comunidad, sin embargo, técnicamente no son iguales.

JSON es un estándar para 'transmisión de datos usando pares de clave valor', casi todos los lenguajes tienen soporte para este estándar, por lo que comúnmente se utiliza para intercambiar datos entre aplicaciones.

La expresión de inicialización de objetos nos permite crear objetos en JavaScript, que son una colección de pares de clave:valor, en este sentido igual que JSON.

La MDN destaca estas principales diferencias entre uno y otro.

JSON sólo permite la definición de propiedades con la sintaxis “propiedad”: valor donde podemos distinguir que el identificador de la propiedad está rodeado de comillas dobles, mismas que no son necesarias en JavaScript.

JSON no soporte la sintaxis shorthand.

JSON no permite propiedades computadas.

En JSON no podemos tener funciones como valor, sólo cadenas, números, booleanos, null, arreglos y otros objetos JSON.

En JSON “__proto__” es un nombre de propiedad válido, mientras que en JavaScript tiene una función especial.

En conclusión, ¿es esto JSON o no?-> R: NO.

let usuario = {
  nombre: "Mariana",
  edad: 29
}

En términos prácticos sí, es útil referirse a esta estructura como JSON luego de que todos la identificamos como tal.

¿De qué me sirve saber entonces que esto no es JSON en el sentido más estricto y técnico?

let usuario = {
  nombre: "Mariana",
  edad: 29
}

let usuario = {
  "nombre": "Mariana",
  "edad": 29
}

*/

/*
🎯🎯 Shorthand syntax: Sintaxis taquigrafica.
La shorthand sintax nos permite asignar propiedades nuevas a un objeto pasando sólo una variable, en lugar de un par clave valor.

let nombre = "Maria";
let user = { nombre: nombre};
user = {nombre};

*/

let nombre = 'Andres';
let user = {
  nombre,
  edad: 29,
};
console.log(user);

/*
La sintaxis shosthand en JavaScript es definir solo la variable, no el par clave:valor:

let user = {
  nombre,
  edad: 29
}

console.log(user);

El lenguaje asígna como son iguales ese valor.

Funciona tambien con los metodos, en ves de hacerlo asi:

let user = {
  nombre,
  edad: 29,
  saludar: function(){
    console.log("Hola");
  }
}

Lo hariamos asi:

let user = {
  nombre,
  edad: 29,
  saludar(){
    console.log("Hola");
  }
}

Recuerda que no existe una relación entre la variable y la propiedad.

*/

/*
🎯🎯 Computed properties: Tenemos que usar los corchete [] como clave:
JavaScript permite usar el resultado de una expresión como nombre de una propiedad si la colocas dentro de un par de corchetes en la definición de un objeto.

let key = "apellido";
let usuario = {
  nombre: "Michael",
  [key]: "Jordan"
}

*/

let objeto = {
  edad: 20,
  // Aqui vemos como ejecutamos código dentro de los []: Esto es un propiedad computada:
  ['annio_' + new Date().getFullYear()]: 'Todo es posible',
};

console.log(objeto);

/*
🎈🎈 Es una clave dinamica. Dentro de los [] podemos ejecutar codigo JS
*/

/*
Ejercicio: Mejorar el juego del numero mágico:

En una sesión de juego puede jugarse más de una vez.
- Entre cada juego se guardará la cantidad de intentos que tomó para poder adivinar el número.
- Al finalizar cada juego deberás preguntar al usuario si desea jugar otra vez o finalizar la sesión.
- Al finalizar una sesión de juego, deberá imprimir el número de intentos que le tomó al juego que se resolvió más rápido.

*/
