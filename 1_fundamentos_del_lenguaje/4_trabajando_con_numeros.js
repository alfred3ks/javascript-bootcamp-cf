/*
🎯🎯 Trabajando con números:
Un numero es un valor que puede usarse en una operación matemática, en JavaScript estos valores son de tipo number.
- No son objetos,
- No pueden tener propiedades,
- Son immutables.
*/

let number = 10;
number = 20;

// Aqui la variable apunta a un nuevo valor, pero el valor 10 es inmutable no ha cambiado, lo que ha cambiado es a donde apuntamos la variables que ahora es 20. Cambiamos la referencia de la variable.

// Esta caracteristica del lenguaje (immutabilidad) permite que, si multiples variables estan usando un mismo numero, todas apunten al mismo lugar de memoria.

// En JavaScript los primitivos tienen su equivalente a objeto. Tenemos el primitivo number y tenemos el objeto Number(). Esto existe es para que podamos ejecutar métodos sobre los primitivos.

let data = new Number(10);
console.log(typeof data); // object

let dataString = data.toString();
console.log(dataString); // '10'
console.log(typeof dataString); // string

// Los números van de la mano de los operadores aritmaticos, suma, resta, multiplicación, división, resto o módulo.

console.log(10 + 45); // 55
console.log(10 - 45); // -35
console.log(10 * 45); // 450
console.log(100 / 45); // 2.2222222
console.log(10 % 2); // 0
console.log(10 % 3); // 1

// Tambien para diversos tipos de operaciones tenemos la libreria Math() con sus metodos estaticos:

console.log(Math.random());
console.log(Math.pow(10, 2));
console.log(Math.PI);

/*
⛳😎 Vamos a realizar un segundo ejercicio:
Cálcula la edad de una persona, solicita el año de nacimiento e imprime la edad.
*/

const annioNacimiento = 1975;
const annioActual = new Date();

const getAge = annioActual.getFullYear() - annioNacimiento;

console.log('El usuario tiene ' + getAge + ' años.');
