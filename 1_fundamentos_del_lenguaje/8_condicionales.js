/*
🎯🎯 Condiciones: if...else:
Una condición es una declaración que nos permite evaluar una expresion booleana e invocar un conjunto de declaraciones (código) si la expresion es verdadera y otra si la expresión es falsa. Los condiconales en JavaScript comienza con la palabra reservada if()

Sintaxis:

if(<expresion-booleana>){
  codigo...
} else {
  codigo...
}

Tambíen podemos ejecutar declaraciones else if que nos permite evaluar mas condiciones si la primera es falsa.

if(<expresion-booleana>){
  codigo...
} else if(<expresion-booleana>) {
  codigo...
} else {
  codigo...
}

*/

/*
🎯🎯 El operador ternario: ⛳😎
El operador ternario es el único operador del lenguaje que trabaja con 3 elementos:

- Una condición a evaluar,
- Una expresión a ejecutar si la condición es true,
- Una expresión a ejecutar si la condición es false.

Simtaxis:

<condicion> ? true : false;

Muy usado en react.

*/

const isDev = true;
const info = isDev ? 'Es developer' : 'No es devekoper';
console.log(info);

const ageTour = 18;
const tour = ageTour >= 18 ? 'Puede ir al tour' : 'No puede ir al tour';
console.log(tour);

/*
⛳😎 Vamos a realizar un sexto ejercicio:
Has consumido X cantidad de megas en wikipedia, y Y cantidad de megas en memes. El coste de visitar wikipedia es 0.10€ por mega y el de los memes es de 0.05€ por mega.

Si el total consumido es mayor a 100€ imprime por pantalla:
- Consumo demasiado alto,

Si el consumo de ver memes es mayor al de leer wikipedia imprime por pantalla:
- Wow, muchos memes..
*/
