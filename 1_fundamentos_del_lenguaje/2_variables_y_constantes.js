/*
🎯🎯 Variables y Constantes:
Las variables son etiquetas para datos almacenados en la memoria RAM. Una caja que guarda datos.

Las constantes son muy similares a las variables, apuntan o etiquetan un dato almacenado en la memoria RAM, pero no se pueden reasignar.

Para definir una variable en JavaScript podemos usar tres sintaxis alternativas:

numeros = 0;
var numeros = 0;
let numero = 0;

La primera opción nada aconsejada, la tercera es la que usaremos para este curso.

var/let -> palabra reservada del lenguaje.
numero -> identificador,
= -> Operador de asignación,
0 -> Valor de la asignación.

La diferencia entre (var) y (let) recae en un concepto muy importante de JavaScript, el 'scope', o el alcance, del cual hablaremos mas adelante cuando veamos funciones.

Una variable puede declararse sin un valor inicial cuando usamos var/let, en este caso el valor apunta a undefined. Esto es una declaración sin asignacion y JavaScript es quien asigna el valor de undefined.

let numero;

Cuando una variable no utiliza ni var ni let, esta se convierte en una variable global. NOTA: NO SE RECOMIENDA DECLARAR VARIABLES ASI DE ESTA MANERA.

numero = 0;

Para definir una constante utilizamos la palabra reservada const.

const PI = 3.1416;

La sintaxis es muy similar a la definición de variables, lo unico que cambia entre sus contraparte de variables es que si intentas reasignar una constante recibiras un error.

Las constantes pueden representar un beneficio de rendimiento en la ejecución de tu código, aunque no uno critico.

¿Entonces, cuando usar variables o cuando usar constantes?

Existen distintas olas de pensamiento en lo que respecta al uso de constantes y variables, veamos -dos- de los mas populares.

⛳ Primera opción:
Todo es una constante hasta probar lo contrario. En esta idea, el programador usara const siempre, y solo cambiara una constante a variable si eventualmente en el código identificamos que este dato puede cambiar.

Ventajas:
- Las constantes traen consigo una ventaja en rendimiento.
- Puede ser más fácil razonar el código.

Desventajas:
- Si todas las etiquetas son constantes, la ventaja cognitiva de usarlas se pierde, ya no se puede asumir que una constante no puede modificarse.

⛳ Segunda opción:
Usa constantes sólo para etiquetas que aseguras que numva se reasignaran. En este idea el programador usara comúnmente let, y solo usara const cuando se trate de un valor que no espera se modifique.

Ventajas:
- Las constantes sirven como una referencia semantica del código, entendemos que es dato es constante, su nombre lo indica.

Desventajas:
- El código es mas dificil de razonar si tienes muchas variables.

Pero...

No existe una respuesta definitiva sobre cuál usar, personalmente prefiero la segunda opción.

*/

/*
🎯🎯 Inmutabilidad:
Es el concepto con que nos referimos a un objeto que no puede modificar su valor. En JavaScript los tipos primitivos son estructuras inmutables, algunos ejemplos son los números y las cadenas.

En cambio la estructuras mas complejas como los objetos {}, o los arreglos [], son mutables.

*/

// Veamos unos ejemplos: Vemos que es declarada como ctte pero si vemos como alteramos su valor, porque es un objeto. Aqui esta mutando es el objeto no la ctte.
const user = {};
console.log(user);
user['nombre'] = 'Droid';
console.log(user);

// Veamos con un primitivo: Ahora la variable apunta a otro numero, antes a 20 y luego a 21.
let edad = 20;
edad = 21;

/*
⛳😎 Vamos a realizar un primer ejercicio:
Hagamos un programa de lectura de datos e impresión, solicitaremos que el usuario ingrese un nombre, y luego imprimimos su nombre.

Una cosa que debemos saber, JavaScript carece de mecanismos para leer o imprimir datos por consola. Estos mecanismos lo aporta el entorno de ejecución, en este caso el navegador, por ejemplo promp() y console.log(), o alert()
*/

// const nombre = prompt('Ingresa tu nombre:');
// console.log('El nombre ingresado por el usuario es: ' + nombre);

/*
🎯🎯 Tipado:
Los tipos de datos son anotaciones que agregamos a la información de nuestro programa para que el interprete o el compilador usen y manejen esta información.

Los lenguajes de programación comúnmente se categorizan en dos:
- debilmente tipado,
- fuertemente tipado.

Fuertemente tipados: JAVA
Son muy estrictos con los tipos y generamente requieren que el tipo de dato se defina al declarar la variable, asi como que la variable no se reasigne a un dato de distinto tipo.

Debilmente tipado:
No son tan estrictos, por lo que una variable puede cambiar y apuntar a valores de distintos tipos.

JavaScript es debilmente tipado.

Algunos lenguajes incluyen tipado dinamico, en cuyos casos no es necesario especificar en el código de que tipo sera una variable, el interprete puede asignar y definir el tipo de dato de la variable en tiempo de ejecución.

JavaScript es de tipado dinámico.

*/
