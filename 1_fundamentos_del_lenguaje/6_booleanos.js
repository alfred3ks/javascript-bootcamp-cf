/*
🎯🎯 Booleanos: true o false.
Este tipo de dato significa que su valor únicamente puede ser True o False, verdadero o falso.

Decimos que una expresion es booleana cuando su resultado es true o false, y se componen de operadores de comparación, operadores lógicos y otros tipos de booleanos.

Para trabajar con expresiones booleanas podemos usar los siguientes operadores:

- === -> Igual a ->  5 === 10 -> false,
- !== -> Diferente de -> 5!== 10 -> true,
- > -> Mayor que -> 5 > 10 -> false,
- < -> Menor que -> 5 < 10 -> true,
- >= -> Mayor o igual 5 >= 10 -> false,
- <= -> Menor o igual 5 <= 10 -> true.

Tambien podemos usar los operadores lógicos:

- && -> and -> true si ambas expresiones son verdaderas, sino false:
5 > 2 && 3 < 10 -> true,

- || -> or -> true si al menos una de las expresiones es verdadera, sino false: 5 > 3 || 3 > 10 -> true,

- ! -> Negación -> Invertimos la expresión booleana:
!5 > 10 -> true,

- ?? Evalua el valor o la expresión de la izquierda, si es nula o undefined retorna el valor de la derecha.

*/

/*
🎯🎯 Truthy y Falsy: Muy poco visto en otros lenguajeas de programación:
Decimos que un valor es Falsy cuando su representación booleana es falso. Los valores truthy por su parte son todos aquellos que no sean falsy, es decir que su presentación boolena sea verdadero.

Vamos a ver los valores que son falsy en JavaScript:

- NaN,
- null,
- 0,
- -0,
- "",
- false.
- undefined

Cuando usamos un dato que no es booleano, donde se espera uno, JavaScript utilizara type coerción para convertirlo a su valor truthy o falsy.

Osea para determinar si un valor es truthy o falsy el lenguaje usa type coerción.

*/

if ('') {
  console.log('Evaluando booleanos.');
}

// En este ejemplo vemos como JavaScript hace el type coerción, genera el valor falsy del cero.

/*
⛳😎 Vamos a realizar un cuarto ejercicio:
Determinar si las siguientes expresiones son falsas o verdaderas.

true -> evalua a true,
false -> evalua a false,
!true -> evalua a false,
0 -> evalua a false,
"" -> evalua a false,
!!"" -> evalua a false,
null || 'null' -> evalua a true,
[0] -> evalua a true,
[] -> evalua a true,

*/

// Evaluando valores truthy o falsy:
if ([0]) {
  console.log('Soy truthy');
} else {
  console.log('Soy falsy');
}
