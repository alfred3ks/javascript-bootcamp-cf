/*
🎯🎯 Tipos de datos en JavaScript:
En JavaScript existen 6 tipos de datos primitivos:

- string -> cadenas de texto,
- number -> números,
- boolean -> true o false,
- undefined -> ausencia de valor,
- symbol,
- null.

null es un caso raro, en la práctica es un objeto.

Para saber el tipo de una variable tenemos una propiedad:

typeof
*/

// Veamos ejemplos: De tipado:
let nombre = 'Helena';

// Podemos saber el tipo de una variable con typeof:
console.log(typeof nombre); // string

// Existen funciones para cambiar el tipo: De string a number.
let age = '20.5';
age = parseInt(age);
console.log(typeof age); // number

/*
🎯🎯 Type coerción:
Cuando en nuestro código interactuan dos tipos de datos distintos en una misma operación, JavaScript hara una conversion implicita de datos a la que llamaremos type coerción.
*/

console.log('30' + 2); // 302
console.log(10 + '5'); // 105

// En estos ejemplos el lenguaje realiza esa conversion implicita, nosotros no lo vemos, convierte en string al numero y los une, concatena.

// Tenemos el caso del doble igual, para comparar, ==, solo compara los tipos, entonces el lenguaje hace su conversión implicita y luego los compara los tipos y como son iguales dara true.

console.log(10 == '10'); // true
console.log([] == 0); // true

// Como vemos JavaScript tiene estas particularidades, si no queremos que el haga sus conversiones de manera implicita seremos nosotros los que debemos hacerlo de manera explicita.

console.log(parseInt('30') + 2); // 32

// Igual para cuando hagamos comparaciones usaremos la comparación estricta: No existe type coersion, compara valor y tipo.

console.log(10 === '10'); // false
