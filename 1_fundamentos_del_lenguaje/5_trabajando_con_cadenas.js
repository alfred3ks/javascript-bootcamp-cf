/*
🎯🎯 Trabajando con cadenas, string:
En términos técnicos, las cadenas son secuencias ordenadas de 0 o mas valores unsigned int de 16 bits, usados para representar texto.
Historicamente en ciencias de la computación nos hemos referido a las representaciones de texto como cadenas o string, porque son una colección de elementos, en un orden especifico.

En Javascript, a diferencia de otros lenguajes, no existe un tipo de dato para un caracter, un caracter es una cadena de un elemento.

Este primitivo tambien tiene su equivalente a objeto, String()
*/

const cadena = new String('test');
console.log(cadena);
console.log(typeof cadena);

const userName = 'Cody';
// Aqui vemos type coersión, el lenguaje transforma al equivalente a objeto el string para poder usar el método:
console.log(typeof userName);
console.log(userName.length);

/*
🎯🎯 Concatenación y interpolación:
Concatenar es el término que usamos para la operación de unir dos o más cadenas una tras la otra, en el proceso de concatenación el inicio de una cadena se añade al final de otra. Normalmente se usa el operador de la suma, pero tambien se puede usar el metodo .concat().

Tambien existe la interpolación:
Interpolar es el proceso de evaluar un string que contiene uno o varios marcadores que seran sustituidos por otro valor.
*/

const chainOne = 'Hola,';
const chainTwo = ' ¿como estas?';
const chainAll = chainOne + chainTwo;
console.log(chainAll);

console.log(chainOne.concat(chainTwo));

// Veamos interpolación: Con los template literals: `` y ${}:
console.log(`Este es una interpolación de string: ${chainAll}`);

/*
⛳😎 Vamos a realizar un tercer ejercicio:
Solicita al usuario:
- Su nombre,
- 3 calificaciones.

Cálcula el promedio de las tres calificaciones y imprime por pantalla el siguiente mensaje:

Hola <nombre> tu promedio calculado es <promedio>
*/

const userData = 'Maria';
const calf1 = 7;
const calf2 = 7.4;
const calf3 = 9.3;

const promedio = Math.round(calf1 + calf2 + calf3 / 3);
console.log(promedio);

console.log(`Hola ${userData} tu promedio calculado es ${promedio}`);
