/*
🎯🎯 undefined y null:
undefined es un tipo de dato y un valor, el valor undefined es el único del tipo undefined.
null es un objeto. Indica la ausencia de valor,
*/

let unde;
console.log(typeof unde); // undefined

unde = null;
console.log(typeof unde); // object

/*
⛳😎 Vamos a realizar un quinto ejercicio:
Contesta las siguientes preguntas:

- ¿Porque undefined == null es true?
El lenguaje hace type coerción, porque los dos son falsy. Recuerda lo del doble igual donde el lenguaje hace type coerción.

*/

if (undefined == null) {
  console.log('Soy truthy');
} else {
  console.log('Soy falsy');
}
