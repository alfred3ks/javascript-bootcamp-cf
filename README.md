# Bootcamp de JavaScript de Código Facilito - 2023.

# Indice del curso:

🎯🎯 Breve historia.

🎯🎯 1-Fundamentos del JavaScript:
- Variables y Constantes,
- Tipos de datos y coerción de tipos,
- Trabajando con números,
- Trabajando con cadenas,
- Booleanos.
- undefined y null,
- Condicionales.

🎯🎯 2-Ciclos y Funciones:
- Ciclos,
- Funciones,
- Alcance o scope,
- Hoisting,
- Parámetros y argumentos,
- Funciones como objetos de primera clase.

🎯🎯 3-Arreglos y objetos:
- Fundamentos de arreglos,
- High Order Functions,
- Funciones de arreglos,
- Objetos / JSON,
- Object initializers.
- Destructuring assignment,
- Spread syntax,
- Operator rest.

🎯🎯 6-Programación asíncrona:
- Asincronismo,
- Event Loop,
- Métodos asincronos:
  - Callback,
  - Promesas,
  - Async Await.

🎯🎯 8-Manejo del DOM:
- Que es el DOM,
- Terminología,
- Obtener un nodo,
- Que hay en un nodo,
- modificar un nodo,
- Como usar Javascript en una web.

## Alfredo Sánchez - @alfred3ks
