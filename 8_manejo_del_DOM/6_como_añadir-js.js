/*

Como añadir codigo js a nuestro web.
Existen dos formas posibles de asociar código js a una pagina web.

- Usando la etiqueta script en cualquier punto de la página.
  <script>
    alert('Hola)
  </script>
- Lo mas comun y recomendado, incluir un archivo externo con script y atributo src.

  <script src="main.js"></script>

Los lugares mas comunes de colocar la etiqueta script:
Considerando que el código va a ejecutarse tan pronto el navegador lea la etiqueta script, debemos colocarla cuando querramos que se ejecute el código.

Recomendacion hacerlo al final.

*/
