/*

Veamos la siguiente terminología:

NODO:
Representan a nuestras etiquetas HTML, en su representación virtual son objetos de Javascript, por lo que tienen métodos y propiedades, a los que ademas se les pueden enlazar eventos de interacción.

NODO RAIZ:
El nodo raíz del arbol HTML de cualquier web siempre es el mismo, este es document, que es el objeto padre de los objetos restantes. Desde aquí parte el arbol.

PADRE / PARENT:
A excepción del nodo raíz, todos los nodos tienen un padre, este representa el contenedor en el que estan en HTML.

HIJOS / CHILDREN:
Todos los elementos que contienen una etiqueta, se representan como sus hijos en el árbol.

SIBLINGS / PARIENTES:
Decimos que los nodos que corpante un mismo padre son parientes o siblings.

ANCESTROS:
El nodo padre, el padre del padre, y en todos los nodos que estan en la cadena de sucesión hasta llegar al nodo raiz.

SUBTREE:
Es una porción del DOM.

*/
