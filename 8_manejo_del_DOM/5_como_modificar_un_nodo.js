/*

Modificar un nodo.

Existen muchos métodos para alterar un nodo.

Que podemos modificar de un nodo:

- Modificar el valor de sus atributos,
- Modificar sus clases/estilos,
- Modificar su contenido,
- Modificar el HTML del nodo.

*/

/*
Para modificar sus atributos: Tenemos dos formas:
- Por medio de la nomenclatura del punto al atributo y asignarle un nuevo valor,
- Por medio del metodo setAttribute(), recibe dos argumentos, el primero el nombre de atributo a modificar y segundo el valor del atributo.

Debemos usar la segunda forma cuando tengamos mas atributos que queramos modificar. Por medio de la nomenclatura del punto solo podemos modificar propiedades asociadas al elemento.

Nuestros nodos en elDOM convenientemente añaden propiedades para algunos atributos del elemento, pero no lo hacen para todos.
*/
const enlace = document.querySelector('a:first-child');
console.log(enlace);

enlace.href = '/suscripcion';
enlace.setAttribute('href', '/cursos');

enlace.setAttribute('nombre', 'Maria');

/*
Para modificar sus clases/estilos:
Usando la propiedad style podemos modificar los estilos de ese nodo: Luego por medio de la momenclatura del punto aplicamos los estilos. Estos estilo se añaden de forma inline sobre el nodo. Las propiedades css de mas de una palabra debemos usar camelcase.

Ahora si queremos añadir una clase lo podemos hacer anañdiendo dicha clase por medio del atributo className.

Tambien tenemos classList el cual nos permite agregar y quitear otras clases, el anterior solo agregar la que le agregamos pero nos quita las demas que tengamos.

Con toggle podemos hacer que si la clase la tiene se la quita sino se la pone.

*/

// enlace.style.color = 'red';
// enlace.style.background = 'black';
// enlace.style.padding = '10px 2px';
// enlace.style.marginLeft = '30px';

const li = document.querySelector('li');
console.log(li);

li.className = 'otra-clase-red';

li.classList.add('otra-clase-red');

// li.classList.remove('otra-clase-red');
li.classList.replace('nav-item', 'otra');

console.log(li);

li.classList.toggle('nueva-clase');
li.classList.toggle('otra-clase-red');

/*
Modificar el texto de un nodo:
Hay varias propiedades que podemos usar para obtener/modificar el contenido de un elemento, entre las que tenemos:

- textContet,
- innerText, devuelve solo el texto
- innerHTML.

Cual debemos usar para modificar el contenido de un nodo.

Por temas de seguridad no usar innerHTML. si para leer no para modificar. Ya que nos pueden inyectar codigo por ahi.

*/

console.log(li.textContent);
console.log(li.innerText);
console.log(li.innerHTML);

/*
Por medio de la propiedad childNodes podemos ver todos los hijos que tiene un nodo.

Vemos que hay de varios tipos.
Los nodos text no pueden existir solos tiene que ser hijos de otro nodo.
*/

let el = document.querySelector('#projects');
console.log(el.childNodes);
