/*
¿Que es el DOM?

DOM son las siglas de Document Object Model, y es la representación virtual en programación de nuestro documento HTML.

Gracias a representación virtual del documento, podemos usar un lenguaje de programación como JavaScript para leer y modificar esta estructura.

La forma en que se representa el DOM es en forma de árbol, esta es una estructura muy usada en las ciencias de la computación.

Dentro de ese árbol tenemos uno nodos, estos nodos estan interconectados con nodos cercanos, generando un símil con las relaciones familiares, asi los nodos tienen padres, hijos, hermanos y mas.

*/
