/*

Que hay en un nodo:

Todos los métodos anteriores nos traen un nodo, pero que hay dentro de un nodo.
Todo lo que esta dentro del nodo, toda esa información la podemos obtener usando Javascript.

*/

const a = document.querySelector('a');
console.log(a);
console.log(a.tagName);
console.log(a.getAttribute('href'));
console.log(a.href);
console.log(a.innerText);
console.log(a.innerHTML);

const node = document.querySelector('#about-me');
console.log(node.innerHTML);

/*

Podemos usar para leer un nodo innerHTML o innerText, innerHTML nos retorna un string de todo lo que tiene el nodo, lo vemos en node.

*/
