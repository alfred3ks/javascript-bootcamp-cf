/*

Obtener un nodo del DOM:

Para obtener los diferentes nodos del DOM tenemos distintos métodos:

Todos estos métodos se usan a travez del objeto document.

- document.getElementById(),
- document.getElementByClassName(),
- document.getElementByTagName(),
- document.querySelector()
- document.querySelectorAll()

Para poder ver como usar estos métodos tenemos la carpeta que se llama web_ejemplo.

El objeto document, es a partir de este objeto es donde a aprtir vamos a trabajar con nuestro código de Javascript.

Veamos como usar los diferentes métodos:

*/

/*
📌 document.getElementById():
Con este método podemos acceder a un nodo que usa el atributo id que le pasemos como parametro, como un string. Nos retorna el primer nodo que contenga ese id, al ser los id unicos retornara solo uno.
*/

const aboutMe = document.getElementById('about-me');
console.log(aboutMe);

/*
📌 document.getElementByClassName():
Con este método podemos acceder a un nodo que usa el atributo class que le pasemos como parametro, como un string. Nos retorna todos los nodos que tiene esa clase. Nos retorna un HTMLCollection: No es un arreglo, es un arreglo de tipo HTML. Para poder usar los métodos de los arreglos lo debemos transformar a un arreglo.
*/

const navItem = document.getElementsByClassName('nav-item');
console.log(navItem);
console.log(navItem.length);
console.log(navItem);

const arr = [...navItem];
console.log(arr);

arr.forEach((elm) => {
  console.log(elm);
});

/*
📌 document.getElementByTagName():
Con este método podemos acceder a un nodo como esa etiqueta, se la pasamos como parametro en forma de string. Nos retorna en un HTMLCollection todas las eqtiquetas que existan en el documento.
*/

const section = document.getElementsByTagName('section');
console.log(section);
console.log(section.length);
console.log(section[0]);
console.log(section[1]);
console.log(section[2]);

/*
📌 document.querySelector():
Con este método podemos acceder a un nodo, bien sea por su id, o su class o su etiqueta, es uno de los mas usados en Javascript moderno. Retorna el primer elemento que cumpla con lo que le pasamos por parametro. Retorna un solo nodo.
Este selector de nodo es mas potente, ya que nos permite seleccionar mas concreto lo que queremos.
*/

const navItemQuery = document.querySelector('.nav-item');
const aboutQuery = document.querySelector('#about-me');
console.log(navItemQuery);
console.log(aboutQuery);

console.log(document.querySelector('li:first-child'));
console.log(document.querySelector('li:last-child'));

/*
📌 document.querySelectorAll():
Con este método podemos acceder a todos los nodos, bien sea por su id, o su class o su etiqueta, es uno de los mas usados en Javascript moderno. Retorna el todos los elementos que cumpla con lo que le pasamos por parametro. Retorna un HTMLCollection. Un objeto NodeList. Este si tiene forEach como metodo.
*/

const list = document.querySelectorAll('li');
console.log(list);

list.forEach((data) => {
  console.log(data);
});
