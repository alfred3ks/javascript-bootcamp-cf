/*
🎯🎯 El event loop: 🎈🎈
Es un patron, que permite en el tiempo de ejecución que JavaScript determina funciones de manera asincrona y no bloqueante. Como su nombre lo indica es un ciclo de eventos.

El event loop tiene varios componentes que nos ayudan a ejecutar esas funciones o nuestro codigo dentro del lenguaje.

- ⛳ Call Stack - Pila de llamadas:
Almacena las instrucciones que van a ejecutarse. Es muy similar a un array, se pueden agregar items al final y se remueve el ultimo.

- ⛳ Stack - Pila:
JavaScript es un lenguaje single threaded. Existe un solo thread que ejecuta el código. Por lo tanto cuenta con un call stack. Al haber un solo thread es importante no escribir código bloqueante.

- ⛳ Memory Heap:
Region de memoria libre. Se dedica al alojamiento dinámico de objetos.
Es compartida por todo el programa y controlada por un recolector de basura que se encarrga de liberar aquello que no necesita.

- ⛳ Web API:
Es una libreria o biblioteca, que nos provee el navegador para potencias las habilidades de JavaScript. Le aportamos a JavaScript super poderes.
console,
setTimeout,
DOM,
setInterval. etc.

- ⛳ Callback Queue o Macrotask Queue: Cola de tareas:
Es un mediador, donde se enváa el código asíncrono y espera a ser ejecutado. Una vez el call stack esta vacio, las tareas del callback queue pasan al call stack.
Aqui vemos como funciones:
🎈setTimeout.

- ⛳ Microtask Queue: Cola de microtareas:
Es un mediador, como el callback queue, pero las tareas tienen mayor prioridad. Para este caso cuando trabajamos con promesas.
🎈Promises.

En esta web podemos ver el funcionamiento de todo esto:

https://www.jsv9000.app/

*/
