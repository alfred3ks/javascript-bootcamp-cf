/*
- 🎯🎯 Métodos asíncronos:

🎈 Callback:
JavaScript lo creo de manera asincrono, pero los 🎈callback pueden ser sincronos🎈 tambien, OJO con esto:
Es una función que se pasa a otra función como parametro de netrada y esta se ejecuta en un tiempo determinado.

🎈 A continuación tenemos un callback sincrono:
function saludar(nombre) {
  alert("Hola " + nombre);
}

function procesarEntradaUsuario(callback) {
  let nombre = prompt("Por favor ingresa tu nombre.");
  callback(nombre);
}

procesarEntradaUsuario(saludar);

🎈 A continuación vemos una función que usa un callback asincrono: setTimeout es una funión asíncrona que usa una funcion como callback que se ejecutara un tiempo segun le asignemos.

setTimeout(()=>{
  console.log('Hola);
}, 2000)

Lo vemos en la MDN:
https://developer.mozilla.org/es/docs/Glossary/Callback_function

*/

// Tenemos un ejemplo de combinación de operaciones sincronas y asincronas:
console.log('INICIO');

setTimeout(() => {
  console.log('Dentrol del timeout');
}, 2000);

console.log('FIN');

//🎈 Ejercicio de la alarma:
setTimeout(() => {
  console.log('¡Iniciar alarma!');
}, 0);

setTimeout(() => {
  console.log('¡Alarma!');
}, 500);

function alarma(mensaje, tiempo) {
  setTimeout(() => {
    console.log(mensaje);
  }, tiempo);
}

alarma('Me despierto', 2000);
alarma('Me ducho', 5000);
alarma('Desayuno', 3000);

/*
🎈 Un callback es es una funcion que se pasa como argumento a una operacion asincrona con la espectativa que dicha funcion sea ejecutada una ves que la operacion termine.

Imagina que cuando delegamos una operacion asincrona a la cola de tareas tambien añadimos una funcion que se debe ejecutar cuando la operacion asincrona termine, eventualmente el event loop se entera que la operacion asincrona ha terminado y ejecuta esta funcion a la que llamamos callback.

Existen distintas operaciones en JS que se ejecutan de manera asincrona, por ejemplo el trabajo con archivos, lectura escritura de archivos, peticiones a la red a APIs, hacia paginas web, consultas con un BD, entre otras.

Para este ejemplo vamos a utilizar una libreria que nos permite leer paginas de la red.

Vamos a instalar la libreria request.

Tienes que crear el package json y luego instalar request para que funcione.

npm init -y

Este paquete esta obsoleto, no recibe actualizaciones pero solo lo usaremos para demostrar una operacion asincrona.

Lo instalamos:

npm install request -D

Lo he probado con repplit y funciona tambien aho sin instalar nada.

*/

// let request = require('request');

// request('https://www.google.com', function () {
//   console.log('Termine la peticion.');
// });

// console.log('Ahora me ves...😎');

// La funcion request hace una peticion al sitio web, solo cuando esta ha terminado luego ya ejecuta la funcion que se pasa como callback con el msj de termine la petición. Lo vemos que asi sucede porque vemos que JavaScript se sigue ejecutando y el siguiente msj de Ahora me ves sale primero que el otro msj

/*
🎯🎯 Promesas: {}
Es un objeto que representa la resolución o el fracaso de una operación asincrona.

Las promesas pasan por distintos estados:

- fullfiled: Completada, significa que la promesa se completo con exito.
- rejected: Rechazada, significa que la promesa no se completo con exito.
- pending: pendiente, significa que es el estado de la promesa cuando la operacion no ha terminado, aqui decimos que la promesa no se ha compleado.
- settled: finalizada, cuando la promesa termino bien sea con error o con exito.

Las promesas internamente usan callbacks.

Para saber que callbacks usar usamos los siguientes métodos:

.then(resolve) -> Ejecuta el callback resolve cuando se cumple la tarea.
.catch(reject) -> Ejecuta el callback reject cuando la tarea fue rechazada.
.finally(end) Ejecuta el callback end cuando finaliza todo bien sea aceptada o rechazada.

*/

// Veamos un ejemplo:
const promesa = new Promise((resolve, reject) => {
  // Aceptamos la promesa:
  setTimeout(() => {
    resolve('Listas, promesa resulta!!!');
  }, 1000);

  // Rechazamos la promesa:
  setTimeout(() => {
    reject(new Error('Error'));
  }, 2000);
});

// Para consumir la promesa necesitamos los métodos:
promesa
  .then((result) => {
    console.log(result);
  })
  .catch((err) => {
    console.log(err);
  })
  .finally(() => {
    console.log('Fin promesa...');
  });

/*
🎯🎯 Async await:
Son una mejora a las promesas y son Sugar Syntactic, azúcar sintáctico.
Async convierte una funcion en asincrona y retorna una promesa.

Para consumirla debemos usar la instrucción await. El cual espera a que se resuleva la promesa.

Para manejar los errores necesitamos usar las instrucciones:
  try{}
  catch{}

*/

// Veamos un ejemplo:
function resolveAfter2Seconds() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      // resolve('Resolved promise');
      reject('No va...');
    }, 2000);
  });
}

async function asyncCall() {
  console.log('calling');
  try {
    const result = await resolveAfter2Seconds();
    console.log(result);
  } catch (err) {
    console.log(new Error(err));
  }
}

asyncCall();

// Tenemos otro ejemplo consumiendo un endpoint:
let showGitHubInfo = async () => {
  let url = 'https://api.github.com/users/alfred3ks/repos';
  let response = await fetch(url);
  let json = await response.json();
  console.log(json);
};

setTimeout(showGitHubInfo, 7000);
