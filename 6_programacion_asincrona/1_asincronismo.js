/*
🎯🎯 Asincronismo:
En el tema del asincronismoe n Javascript hay dos terminos que tenemos que tener muy claro.

- Bloqueante,
- No bloqueante,
- Paralelismo,
- Concurrencia,
- Mono-thread,
- Multi-thread,
- Sincrono,

🎈🎈 Bloqueante:
Son operaciones que bloquean la aplicación, y hasta que no se completa la operación no devuelve el control. Por ejemplo el alert().

🎈🎈 No bloqueante:
Son operaciones que devuleven el control inmediatamente.

🎈🎈 Paralelismo:
Es cuando una operación se ejecuta al mismo tiempo que otra.

🎈🎈 Concurrencia:
Es cuando una operación puede ser simultánea a otra.

🎈🎈 Mono-thread:
Un único hilo de ejecución.

🎈🎈 Multi-thread:
Varios hilos de ejecución.

🎈🎈 Sincrono:
Se ejecuta en secuencia. Una sentencia se ejecuta detras de otra. Asi funciona JavaScript, por defecto, y muchos otros lenguajes de programación.

Ejemplo de código sincrono:
console.log('Hola');
console.log('¿Como estas?');
console.log('Saludos!!!');

De JavaScript se conoce que es sincrono bloqueante, pero tambien es asincrono no bloqueante.

🎈🎈 Asincronismo:
Una operación se puede ejecutar estando otra en proceso. Gracias al event loop el lenguaje puede gestionar o ejecutar multiples tareas en paralelo no bloqueante.
*/
