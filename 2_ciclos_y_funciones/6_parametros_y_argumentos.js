/*
🎯🎯 Parámetros y argumentos:
Para recibir información en una función usamos parámetros, y es a través de estos es que una función debe recibir toda la información que necesite del exterior.

El parametro es la variable que colocamos dentro de los parentesis de la declaracion de la función. Los parámetros es una variable que debemos llenar. Los parámetros se meten separados por comas si tenemos mas de uno.

Caracteristicas de los parametros en JavaScript:
- No tienen ningún tipo, es decir, un parametro puede recibir un argumento de cualquier tipo.
- Al llamar la función no se revisa la cantiad de parámetros enviados, puedes enviar mas o menos argumentos de los listados.

Los argumentos es la información que pasamos al ejecutar la función.
Los argumentos los enviamos al llamar la funcion, es el valor.

function saludar(<p1>, <p2>, <p3> ){
  console.log('Hola);
}

saludar(<arg1>, <arg2>, <arg3>);

*/

/*
🎯🎯 El objeto arguments: []
Los argumentos se asociaran para cada parámetro listado en el orden especificado, y todos los parámetros listados se agruparán en el objeto arguments.

Esto es una caracteristica del lenguaje que lo hace más flexible.

Es un objeto de tipo Array, por lo que podras acceder a sus elementos por su indice, obtener la longitud e iterarlo.

*/

function sumarTodos(num1, num2) {
  console.log(arguments); // Vemos todos los argumentos enviados en un []
  console.log(arguments[4]);
  console.log(arguments[5]);
  return num1 + num2;
}

console.log(sumarTodos(2, 4, 5, 78, 45, 12));

/*
🎯🎯 Valores por defecto:
Por defecto un parametro que no recibe un argumento es undefined.
Podemos asignar otro valor por defecto en la definición de la función.

function saludar(<p1> = 10, <p2> = 20, <p3> = 30){
  console.log(p1, p2, p3); // 10, 20, 30
}

saludar();
*/

const saludarUser = (nombre, mensaje = 'Hi') => {
  return `${mensaje}, ${nombre}.`;
};

console.log(saludarUser('Pepe'));
console.log(saludarUser('Maria', 'Hello'));

/*
Los parámetros por defecto deben aparecer despues de los que parámetros sin valor por defecto.
*/

/*
🎯🎯 Pase por VALOR y pase por REFERENCIA:
IMPORTANTE!!! 🎈🎈🎈🎈
En JavaScript, un argumento puede ser enviado por valor o por referencia.
Esta diferencia influye en que sucede con el argumento original despues de la ejecución de la función.

- Decimos que un valor a sido enviado como REFERENCIA, cuando el parámetro apunta a la misma dirección que el argumento original, cualquier modificación sobre este valor afecta al argumento original.

- Decimos que pasamos por VALOR cuando el argumento es copiado en una dirección distinta para el parametro, cualquier modificación al parametro no afecta el valor orginal.

NOTA:🎈🎈🎈🎈
Cualquier valor que no sea un objeto {}, o un arreglo [], es pasado por valor, esto incluye a los números, cadenas, booleanos, etc.

Los objetos y arreglos se pasan por referencia. Simple.

*/

// Pase por Valor:
let edad = 10;
function modificadorValor(numero) {
  numero = 28;
  return numero;
}
console.log(edad); // 10
modificadorValor(edad);
console.log(edad); // 10

// Pase por Referencia: Vemos como se modifica el original: []
let edades = [23, 45, 22, 17];
function modificadorReferencia(arr) {
  arr[4] = 22;
  arr[5] = 45;
  return arr;
}

console.log(edades); // [23, 45, 22, 17]
modificadorReferencia(edades);
console.log(edades); // [23, 45, 22, 17, 22, 45] Se modifico...
