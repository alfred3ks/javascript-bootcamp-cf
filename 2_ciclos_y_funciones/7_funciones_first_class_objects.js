/*
🎯🎯 Funciones como objetos de primera clase: FIRST CLASS OBJECTS:
En programación decimos que cuando un tipo de dato puede ser asignado a una variable, retornado o enviado como argumento se trata de un FIRST CLASS SITIZEN, en español, un ciudadano de primera clase.

Caracteristicas de un FIRST CLASS OBJECTS:
- Debe ser posible retornarlo,
- Debe ser posible asignarlo a una variable,
- Debe ser posible enviarlo como argumento.

Los números, las cadenas, los booleanos, los arreglos, las funciones y los objetos en JavaScript son considerados de primera clase. Practicamente todo.

Usaremos funciones como argumentos cuando queramos:
- retrasar esta ejecución,
- Preparar el entorno para ejecutar la función,
- Ejecutar la función asincronamente.

Podemos escribir una funcóon que retorne a otra:

function build(){
  function a(){};
  return a;
}

Muy comun en React: Retornar una función:
const withHigherOrderComponent = (Component)=>{
  return (props) => <Component {...props} />;
}

// Vemos como enviamos una función: Normalmente una función anónima:
setTimeout(()=>{
  console.log('Hola);
}, 1000)

*/
