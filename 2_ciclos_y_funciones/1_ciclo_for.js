/*
🎯🎯 Ciclos:
En lo general los ciclos son declaraciones que nos permiten repetir un grupo de instruciones hasta que una condición se cumpla, algunos otros ciclos nos permiten en cambio recorrer una estructura definida.

Tenemos los siguientes en JavaScript:

- El ciclo for,
- El ciclo for in,
- El ciclo for of,
- El ciclo while,
- El ciclo do while.

*/

/*
🎯🎯 El ciclo for:
Para crear un ciclo for debemos usar la declaración for.
Se compone de la siguiente estructura:

for(<expresion-inicial>; <condición>; <expresion-incremental>){
  codigo a repetir...
}

Un dato curioso es que todos estos elementos son opcionales, por lo que podrias escribir un ciclo infinito asi:

for(;;){
  code...
}
*/

// ciclo for con números de 0 a 10:
for (let i = 0; i <= 10; i++) {
  console.log(i);
}

// ciclo for con numeros de dos en dos:
for (let i = 0; i <= 10; i = i + 2) {
  console.log(i);
}

/*
⛳😎 Ejercicio 1:
Escribe un ciclo que vaya de 3 en 3 y solo imprima los números pares. Debemos recorrer del 0 al 100.
*/

for (let i = 0; i <= 100; i = i + 3) {
  // Imprime los pares:
  if (i % 2 === 0) {
    console.log('número par:' + i);
  }
}

/*
🎯🎯 Ciclo for in: Para recorrer objetos {}
Este ciclo nos permite iterar sobre las propiedades enumerables de un objeto.

let user = {name: 'Frank'};

for(let property in user){
  console.log(property);
}

Veriamos como al recorrer el objeto se imprimiria la propiedad para este caso name.
*/

const user = { name: 'Frank', edad: 29 };
for (let property in user) {
  console.log(property);
}

/*
🎯🎯 Ciclo for of: Para iterar objetos iterable []
Este ciclo nos permite iterar sobre los valores de un objeto iterable.

JavaScript incluye algunos iterables como:

- Array,
- String,
- Map,
-Set.

Para que un objeto sea iterable debe implementar el protocolo de iteración.

Lo que hace interesante a este ciclo es que puede iterar sobre cualquier iterable, incluso tus propios objetos, quienes deberan implementar el protocolo de iteración para convertirse en iterables.
*/

const calificaciones = [10, 20, 30];
for (let calificacion of calificaciones) {
  console.log(calificacion);
}

const cadena = 'Hola como estas';
for (let caacter of cadena) {
  console.log(caacter);
}
