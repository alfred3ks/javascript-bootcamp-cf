/*
🎯🎯 Ciclo while:
El ciclo while se compone de dos elementos, una condición, y una o varias declaraciones. Estas declaraciones se ejecutaran siempre que la condición se cumpla.

while(<condicion>){
  code...
}

Una de las caracteristicas claves de este ciclo es que la condición se ejecutara antes de ejecutar las declaraciones en cada iteración.

Nota: en este tipo de ciclo siempre es importante una variable de control que sera la que parara el ciclo sino tendremos un ciclo infinito.
*/

let contador = 0;
while (contador <= 10) {
  console.log(contador);
  contador++;
}

let numeroRecibido;
let totalAcumulado = 0;
// while (numeroRecibido !== 0) {
//   numeroRecibido = Number(prompt('Dame un numero, o escribe 0 para terminar'));
//   totalAcumulado += numeroRecibido;
// }

// console.log('Total acumulado: ' + totalAcumulado);

/*
🎯🎯 Ciclo do while:
Muy similar al ciclo while, secompone de dos elementos, una condión y una o varias declaraciones.

do {
  codigo...
} while(<condicion>)

La principal diferencia entre el ciclo while es que las declaraciones se ejecutan antes de evaluar la condición, por lo que en todo caso aseguramos que se evaluen al menos una vez.
*/

let numero = 10;
do {
  console.log('Hola mundo');
  numero++;
} while (numero <= 10);

/*
⛳😎 Ejercicio 2:
Adivina el numero mágico, considerando esta linea de código con la que puedes obtener un numero aleatorio en JavaScript:

let numeroMagico = parseInt(Math.random() * 100);

Implementa un programa que lo solicite al usaurio ingresar un numero hasta adivinar el número mágico. El programa debera ademas:

- Imprimir: "El numero es mayor" si el usuario ingreso un numero menor.
- Imprimir: "El numero es menor" si el usuario ingreso un numero mayor.
- Imprimir "Felicidades, adivinaste el número" si el usuario ingreso el mismo numero.
- Imprimir: "Tu opción esta fuera de rango", debe ser un número de 0-100 si el usuario ingresa un número menor a 0 o mayor a 100.
- Imprimir: "Error. Solo puedes ingresar números" si el usuario ingresa algo que no sea un número.
*/

// let opcionUsuario;
// let numeroMagico = parseInt(Math.random() * 100);

// while (true) {
//   opcionUsuario = prompt('Ingresa un número de 0 a 100, "q" para salir');

//   if (opcionUsuario === 'q') {
//     break;
//   }

//   opcionUsuario = Number(opcionUsuario);
//   console.log(typeof opcionUsuario);

//   if (opcionUsuario <= 100 && opcionUsuario >= 0) {
//     if (opcionUsuario == numeroMagico) {
//       alert('Felicidades, adivinaste el número mágico');
//       break;
//     } else if (opcionUsuario > numeroMagico) {
//       alert('El numero mágico es menor');
//     } else if (opcionUsuario < numeroMagico) {
//       alert('El número mágico es mayor');
//     } else {
//       alert('Error. Solo puedes ingresar números.');
//     }
//   } else {
//     alert('Tu opción esta fuera de rango, debe ser un número de 0 a 100');
//   }
// }

/*
🎯🎯 break y continue:
La declaración break te permite detener un ciclo dentro de su ejecución.

La declaración continue terminará la iteración actual, pero no detendrá el ciclo.
*/

// let number = 1;
// while (number > 0) {
//   number = Number(prompt('Dame un numero'));
//   console.log(typeof number);
//   if (isNaN(number)) {
//     console.log(number);
//     break;
//   }
// }

// let number = 0;
// while (number < 10) {
//   number++;
//   if (number % 2) continue;
//   console.log(number);
// }
