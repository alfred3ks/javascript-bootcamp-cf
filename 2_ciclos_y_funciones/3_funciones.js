/*
🎯🎯 Funciones: 🎈🎈🎈🎈
Una función es un conjunto de instrucciones que se ejecutan juntas cuando llamamos la funcion.

Por lo general las funciones reciben una serie de argumentos como entrada de datos y retorna un resultado como salida.

El concepto de funciones nos permite encapsular partes de la funcionalidad de nuestro programa independientemente con el proposito de dividir un problema en subproblemas.

Las funciones en JavaScript son muy importantes. Tanto por la flexibilidad y potencial convierten a JavaScript en un lenguaje multiparadigma donde bien puedes organizar tu código en objetos o en funciones.

La sintaxis de una función es la siguiente:

function <nombre-funcion> (){
  code...
}

Para llamar a una función usamos el nombre de la función junto con los argumentos.

nombre-funcion();
*/

function saludar() {
  console.log('Hola dev...');
}

saludar();

/*
Para recibir información en una función podemos listar parametros dentro del parentesis despues del nombre de la funcion.

La función puede retornar un resultado usando la intrucción return.

El valor retornado de la función determinará el resultado de la expresión donde llamamos a la función.

*/

function sumar(num1, num2) {
  return num1 + num2;
}

let resultado = sumar(11, 2);
console.log(resultado);

/*
Existen en JavaScript dos maneras de declarar una función:

- Declaración de función: 🎈🎈
La declaración comienza con la palabra function: Aqui siempre la función debe tener nombre. Esta funcion se podria llamar antes de definirla, eso es por el hoisting, el interprete mueve la funcion a lo mas alto del alcance.

function saludar() {
  code...
}

- Expresión de función: 🎈🎈
La declaración empieza con una variable. Aqui la funcion es anonima, no requiere un nombre como tal, el nombre de la función es la variable. Aqui el hoisting no aplica.

let saludar = function(){
  code...
}

saludar();

🎈🎈 Existe una síntaxis alternativa, cuando usamos expresión de funciones, las funciones flecha o arrow functions.

let saludar = ()=>{
  code...
}

saludar();
*/

let suma = (num1, num2) => num1 + num2;
console.log(suma(20, 10));

/*
Aquí vemos que no tenemos la instrucción return, pero esta arrow function incorporan el return implicito. Suele darse en funciones de una sola linea.
*/

/*
⛳😎 Ejercicio 3:
En tu primer trabajo tech comienzas ganando 10USD por hora, lo que te
permitirá contratar tu membresía de cursos de Código Facilito que
cuesta 13USD, ahora necesitas saber cuántos meses de Código Facilito
puedes contratar con lo ganado en tu primer semana de trabajo. Para
eso:
Crea una función que reciba la cantidad de horas que has trabajado, y
retorne el dinero que has ganado.
Crea una función que reciba la cantidad de dinero que has ganado, y
te diga cuántos meses de Código Facilito puedes adquirir con este
dinero.
Recuerda, no puedes comprar medio mes, ni gastar más de lo que has
ingresado.
Intenta utilizar la sintaxis de flecha y el retorno implícito.
*/

let calcularPago = (horas) => horas * 10;
let mesesPorComprar = (monto) => parseInt(monto / 13);

let dinero = calcularPago(30);
let mesesPremium = mesesPorComprar(dinero);
console.log('Membresia: ' + mesesPremium + ' meses.');
