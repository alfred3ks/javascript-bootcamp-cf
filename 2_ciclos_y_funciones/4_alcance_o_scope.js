/*
🎯🎯 Alcance o scope: Esto es clave en las funciones:
El alcance o scope es una colección de variables, funciones y objetos que estan a tu alcance en algún punto de tu código.

En JavaScript existen varios niveles de alcance:

- Alcance global,
- Alcance local,
- Alcance de bloque,
- Alcance de módulo.

🎈🎈 Alcance global:
Es todo aquello que no ha sido declarado dentro de un bloque o una función.

🎈🎈 Alcance local:
Hace refernencia a todos los elementos disponibles solo para una función. Cada vez que llamas una nueva función "se crea un alcance local para esa función".

Existen muchas razones para las cuales podemos decir que definir variables globales es mala práctica:

- 🎈 Código fuertemente acoplado: Cuando dos elementos de código dependen uno del otro, decimos que estan acoplados, cuando comparten muchos elementos podemos decir que estan fuertemente acoplados.

- 🎈 Cambios inesperados: En grandes proyectos de software la coordinación entre developers, equipo y entre muchos años, es complicada, por lo que el código debe desarrollarse de tal manera que sea modular e independiente.

En lo que respecta a las funciones, una función debe operar solo con la información del alcance local, y todo aquello que necesite del exterior debe comunicarse por argumentos, y todo lo que necesita comunicar hacia el exterior debe ser via retorno.

🎈🎈 Alcance de bloque: let y const
En el alcance de bloque las variables definidas dentro de un bloque de ejecución, solo estan disponibles para ese bloque {}, y no para toda la función.
Si declaramos variables con let y const tendremos variables de bloque {}.

*/

// 🎈🎈 Variable global y local:
let miGlobalVariable = 'Hércules 🙉';
function imprimir() {
  // variable local: solo accesible dentro de la función {} fuera de aquí no existe:
  let miLocalVariable = 29;
  console.log(
    `Hola soy ${miGlobalVariable} y y tengo ${miLocalVariable} años.`
  );
}

imprimir();

// 🎈🎈 Variable de bloque: solo let y const:
function variableBloque() {
  if (true) {
    // Esta variable solo esta disponible en este bloque del if:
    let nombre = 'Pepito';
    console.log(nombre);
  }
  // Aquí falla ya que no existe:
  // console.log(nombre);
}

variableBloque();
