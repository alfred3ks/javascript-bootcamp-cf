/*
🎯🎯 Hoisting de variables:
Para complementar el tema, y una vez que definimos que una de las principales diferencias entre let y var, es que let es de alcance de bloque y var es de alcance global, hablemos del concepto de hoisting para variables.

Como definimos antes, hoisting es el concepto por el cuál, la declaración de funciones y la declaración de variables, se mueven al inicio del alcance, lo cuál nos permite usar estos elementos antes de ser “declarados”.

Veamos un ejemplo:

console.log(x);
var x = 20;

¿Por qué imprime undefined?

Esto sucede porque la declaración de la variable se movió al principio del scope, pero no la asignación, por lo que la variable existe pero aún no tiene valor al momento de la impresión.

Esto también significa que el código a continuación es válido:

x = 20;
console.log(x);
var x;

Ya que al ejecutarse, JavaScript moverá la declaración al inicio.

Ahora, qué pasará si usamos let:
console.log(x);
let x = 20;

En este caso la ejecución del código termina en uno error dado que las variables que usan let, antes de una asignación, tienen como valor “uninitialized”.

En resumen, diferenciamos var y let porque:

- var: siempre se aloja en el scope local dentro de una función, o en el global fuera de una función.

- let: puede alojarse en el scope, local, de bloque, y global, según sea declarada en una función, en un bloque, o fuera de ambos.
- El valor para una variable con var, que no ha sido asignada, es undefined.
- El valor para una variable con let, que no ha sido asignada, es unitialized.
- Las variables también son alojadas al tope del scope, sin embargo, si intentas usar una variable no inicializada, recibirás un error.

*/
